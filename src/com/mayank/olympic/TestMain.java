package com.mayank.olympic;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class TestMain {

    static Main main;
    @BeforeClass
    public static void objectCreation(){
        main = new Main();
    }

    @Before
    public void beforeEachTest() {
        System.out.println("Test started");
    }

   @Test
    public void TestFindNoOfGoldMedalWonByEachYear(){
        List<Athlete> athletes = new ArrayList<>();

        Athlete athlete1 = new Athlete();
        athlete1.setYear("2022");
        athlete1.setName("A");
        athlete1.setMedal("\"Gold\"");
        athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setYear("2022");
        athlete2.setName("A");
        athlete2.setMedal("\"Silver\"");
        athletes.add(athlete2);

        Athlete athlete3 = new Athlete();
        athlete3.setYear("2021");
        athlete3.setName("B");
        athlete3.setMedal("\"Gold\"");
        athletes.add(athlete3);

        Map<String,Integer> goldMedalistdata = new TreeMap<>();
        goldMedalistdata.put("2022 A \"Gold\"",1);
        goldMedalistdata.put("2021 B \"Gold\"",1);

        assertEquals(goldMedalistdata,main.findNoOfGoldMedalWonByEachYear(athletes));
       Assert.assertNotNull(main.findNoOfGoldMedalWonByEachYear(athletes));
       System.out.println("Test1 Passed successfully");

    }

    @Test
    public void TestFindAthleteWonGoldMedalInYear1980AgelessThen30(){
        List<Athlete> athletes = new ArrayList<>();

        Athlete athlete1 = new Athlete();
        athlete1.setYear("1980");
        athlete1.setName("A");
        athlete1.setMedal("\"Gold\"");
        athlete1.setAge("25");
        athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setYear("1984");
        athlete2.setName("B");
        athlete2.setMedal("\"Gold\"");
        athlete2.setAge("35");
        athletes.add(athlete2);

        Athlete athlete3 = new Athlete();
        athlete3.setYear("1980");
        athlete3.setName("B");
        athlete3.setMedal("\"Gold\"");
        athlete3.setAge("30");
        athletes.add(athlete3);

        Athlete athlete4 = new Athlete();
        athlete4.setYear("1980");
        athlete4.setName("A");
        athlete4.setMedal("\"Gold\"");
        athlete4.setAge("25");
        athletes.add(athlete4);

        Map<String,Integer> goldMedalistlessThen30Year = new TreeMap<>();
        goldMedalistlessThen30Year.put("A 1980 \"Gold\" 25",2);
        goldMedalistlessThen30Year.put("B 1980 \"Gold\" 30",1);

        assertEquals(goldMedalistlessThen30Year,main.findAthleteWonGoldMedalInYear1980AgelessThen30(athletes));
        assertNotNull(main.findAthleteWonGoldMedalInYear1980AgelessThen30(athletes));

        System.out.println("Test2 Passed Successfully");

    }

    @Test
    public void TestFindEventViseMedalListInYear1980() {

       List<Athlete> athletes = new ArrayList<>();

       Athlete athlete1 =new Athlete();
       athlete1.setYear("1980");
       athlete1.setEvent("Swimming");
       athlete1.setMedal("\"Gold\"");
       athletes.add(athlete1);

        Athlete athlete2 =new Athlete();
        athlete2.setYear("1980");
        athlete2.setEvent("Swimming");
        athlete2.setMedal("\"Silver\"");
        athletes.add(athlete2);

        Athlete athlete3 =new Athlete();
        athlete3.setYear("1980");
        athlete3.setEvent("Boxing");
        athlete3.setMedal("\"Gold\"");
        athletes.add(athlete3);

        Athlete athlete4 =new Athlete();
        athlete4.setYear("1980");
        athlete4.setEvent("Boxing");
        athlete4.setMedal("\"Bronze\"");
        athletes.add(athlete4);

        Athlete athlete5 =new Athlete();
        athlete5.setYear("1984");
        athlete5.setEvent("Swimming");
        athlete5.setMedal("\"Gold\"");
        athletes.add(athlete5);

        Athlete athlete6 =new Athlete();
        athlete6.setYear("1980");
        athlete6.setEvent("Running");
        athlete6.setMedal("\"Gold\"");
        athletes.add(athlete6);

        Map<String,List<Integer>> medalList = new TreeMap<>();
        medalList.put("Boxing",new ArrayList<>());
        medalList.get("Boxing").add(0,1);
        medalList.get("Boxing").add(1,0);
        medalList.get("Boxing").add(2,1);

        medalList.put("Running",new ArrayList<>());
        medalList.get("Running").add(0,1);
        medalList.get("Running").add(1,0);
        medalList.get("Running").add(2,0);

        medalList.put("Swimming",new ArrayList<>());
        medalList.get("Swimming").add(0,1);
        medalList.get("Swimming").add(1,1);
        medalList.get("Swimming").add(2,0);

        assertEquals(medalList,main.findEventViseMedalListInYear1980(athletes));
       assertNotNull(main.findEventViseMedalListInYear1980(athletes));

        System.out.println("Test3 Passed Successfully");
    }

    @Test
    public void TestFindGoldWinnerInFootballInEveryOlympic(){
        List<Athlete> athletes = new ArrayList<>();

        Athlete athlete1 = new Athlete();
        athlete1.setSport("\"Football\"");
        athlete1.setMedal("\"Gold\"");
        athlete1.setTeam("USA");
        athlete1.setYear("1984");
        athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setMedal("\"Gold\"");
        athlete2.setSport("\"Hockey\"");
        athlete2.setTeam("UK");
        athlete2.setYear("1980");
        athletes.add(athlete2);

        Athlete athlete3 = new Athlete();
        athlete3.setMedal("\"Silver\"");
        athlete3.setSport("\"Football\"");
        athlete3.setTeam("Canada");
        athlete3.setYear("1970");
        athletes.add(athlete3);

        Athlete athlete4 = new Athlete();
        athlete4.setMedal("\"Gold\"");
        athlete4.setSport("\"Football\"");
        athlete4.setTeam("India");
        athlete4.setYear("1947");
        athletes.add(athlete4);

        Map<String,Integer> goldMedaInFootball = new TreeMap<>();
        goldMedaInFootball.put("1947 India \"Gold\"",1);
        goldMedaInFootball.put("1984 USA \"Gold\"",1);

        assertEquals(goldMedaInFootball,main.findGoldWinnerInFootballInEveryOlympic(athletes));
        assertNotNull(main.findGoldWinnerInFootballInEveryOlympic(athletes));

        System.out.println("Test4 Passed Successfully");

    }

    @Test
    public void TestFindFemaleHighestGoldWinnerAllOlympic() {

       List<Athlete> athletes =new ArrayList<>();

       Athlete athlete1 = new Athlete();
       athlete1.setName("A");
       athlete1.setSex("\"F\"");
       athlete1.setMedal("\"Gold\"");
       athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setName("A");
        athlete2.setSex("\"F\"");
        athlete2.setMedal("\"Silver\"");
        athletes.add(athlete2);

        Athlete athlete3 = new Athlete();
        athlete3.setName("B");
        athlete3.setSex("\"F\"");
        athlete3.setMedal("\"Gold\"");
        athletes.add(athlete3);

        Athlete athlete4 = new Athlete();
        athlete4.setName("A");
        athlete4.setSex("\"F\"");
        athlete4.setMedal("\"Gold\"");
        athletes.add(athlete4);

        Athlete athlete5 = new Athlete();
        athlete5.setName("C");
        athlete5.setSex("\"M\"");
        athlete5.setMedal("\"Gold\"");
        athletes.add(athlete5);

        Map<String,Integer> goldMedalWinnerFemale = new TreeMap<>();
        goldMedalWinnerFemale.put("A",2);
        goldMedalWinnerFemale.put("B",1);

        List<Map.Entry<String, Integer>> highestGoldWinnerFemale= new ArrayList<>(goldMedalWinnerFemale.entrySet());
        highestGoldWinnerFemale.sort(Comparator.comparing(Map.Entry::getValue));

        assertEquals(highestGoldWinnerFemale,main.findFemaleHighestGoldWinnerAllOlympic(athletes));
        assertTrue(!main.findFemaleHighestGoldWinnerAllOlympic(athletes).isEmpty());

        System.out.println("Test5 passed Successfully");
    }

    @Test
    public void TestFindCountryWhichWonHighestMedalIneachOlympic(){

       List<Athlete> athletes = new ArrayList<>();

       Athlete athlete1 = new Athlete();
       athlete1.setYear("2010");
       athlete1.setTeam("USA");
       athlete1.setMedal("\"Gold\"");
       athletes.add(athlete1);

        Athlete athlete2 = new Athlete();
        athlete2.setYear("2010");
        athlete2.setTeam("USA");
        athlete2.setMedal("\"Silver\"");
        athletes.add(athlete2);

        Athlete athlete3 = new Athlete();
        athlete3.setYear("2014");
        athlete3.setTeam("India");
        athlete3.setMedal("\"Gold\"");
        athletes.add(athlete3);

        Athlete athlete4 = new Athlete();
        athlete4.setYear("2018");
        athlete4.setTeam("India");
        athlete4.setMedal("\"Gold\"");
        athletes.add(athlete4);

        Athlete athlete5 = new Athlete();
        athlete5.setYear("2010");
        athlete5.setTeam("USA");
        athlete5.setMedal("\"Silver\"");
        athletes.add(athlete5);

        Map<String,String> highestMeadalWinner = new TreeMap<>();
        highestMeadalWinner.put("2010","USA=3");
        highestMeadalWinner.put("2014","India=1");
        highestMeadalWinner.put("2018","India=1");

        assertEquals(highestMeadalWinner,main.findCountryWhichWonHighestMedalIneachOlympic(athletes));
        assertFalse(main.findCountryWhichWonHighestMedalIneachOlympic(athletes).isEmpty());

        System.out.println("Test6 passed Successfully");
    }

    @Test(expected = NullPointerException.class)
    public void checkException() {
        System.out.println(" NO Exception");
    }

    @Test(timeout = 1)
    public void checktimeLimit() {
        System.out.println("successfully executed within time");
    }

    @After
    public void AfterEachTest() {
        System.out.println("Test ended");
    }

}
