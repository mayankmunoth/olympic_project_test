package com.mayank.olympic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static final int ID = 0;
    public static final int NAME =1;
    public static final int SEX = 2;
    public static final int AGE = 3;
    public static final int HEIGHT = 4;
    public static final int WEIGHT = 5;
    public static final int TEAM = 6;
    public static final int NOC = 7;
    public static final int GAMES = 8;
    public static final int YEAR = 9;
    public static final int SEASON = 10;
    public static final int CITY = 11;
    public static final int SPORT = 12;
    public static final int EVENT = 13;
    public static final int MEDAL = 14;


    public static void main(String[] args) {
        List<Athlete> athletes = getAthleteData();

        findNoOfGoldMedalWonByEachYear(athletes);
        findAthleteWonGoldMedalInYear1980AgelessThen30(athletes);
        findEventViseMedalListInYear1980(athletes);
        findGoldWinnerInFootballInEveryOlympic(athletes);
        findFemaleHighestGoldWinnerAllOlympic(athletes);
        findCountryWhichWonHighestMedalIneachOlympic(athletes);

    }

    public  static Map<String, Integer> findNoOfGoldMedalWonByEachYear(List<Athlete> athletes) {
        Map<String,Integer> goldMedalWinnerPlayerEachYear = new TreeMap<>();

        try{
        for(Athlete athlete : athletes) {
            if(athlete.getMedal().equals("\"Gold\"") ) {
                if(goldMedalWinnerPlayerEachYear.containsKey(athlete.getYear()+" "+athlete.getName()+" "+athlete.getMedal()))
                {
                    goldMedalWinnerPlayerEachYear.put(athlete.getYear()+" "+athlete.getName()+" "+athlete.getMedal(),
                            goldMedalWinnerPlayerEachYear.get(athlete.getYear()+" "+athlete.getName()+" "+athlete.getMedal())+1);
                } else{
                    goldMedalWinnerPlayerEachYear.put(athlete.getYear()+" "+athlete.getName()+" "+athlete.getMedal(),1);
                }
            }
        }
        }catch (Exception e){
            e.printStackTrace();
        }

        return goldMedalWinnerPlayerEachYear;
    }

    public static Map<String,Integer> findAthleteWonGoldMedalInYear1980AgelessThen30(List<Athlete> athletes){
        Map<String,Integer> playerDetail = new TreeMap<>();

        try {
            for (Athlete athlete : athletes) {
                if (athlete.getYear().equals("1980") && athlete.getMedal().equals("\"Gold\"") && Integer.parseInt(athlete.getAge()) <= 30) {
                    if (playerDetail.containsKey(athlete.getName() + " " + athlete.getYear() + " " + athlete.getMedal() + " " + athlete.getAge())) {
                        playerDetail.put(athlete.getName() + " " + athlete.getYear() + " " + athlete.getMedal() + " " + athlete.getAge(),
                                playerDetail.get(athlete.getName() + " " + athlete.getYear() + " " + athlete.getMedal() + " " + athlete.getAge()) + 1);
                    } else {
                        playerDetail.put(athlete.getName() + " " + athlete.getYear() + " " + athlete.getMedal() + " " + athlete.getAge(), 1);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return playerDetail;
    }

   public static Map<String, List<Integer>> findEventViseMedalListInYear1980(List<Athlete> athletes){

       Map<String, List<Integer>> displayMedalList = new TreeMap<>();

       try {
           for (Athlete athlete : athletes) {
               if (athlete.getYear().equals("1980")) {
                   if (displayMedalList.containsKey(athlete.getEvent())) {

                       List<Integer> medaldata = new ArrayList<>();
                       medaldata.addAll(displayMedalList.get(athlete.getEvent()));
                       if (athlete.getMedal().equals("\"Gold\"")) {
                           medaldata.set(0, medaldata.get(0) + 1);
                       } else if (athlete.getMedal().equals("\"Silver\"")) {
                           medaldata.set(1, medaldata.get(1) + 1);
                       } else if (athlete.getMedal().equals("\"Bronze\"")) {
                           medaldata.set(2, medaldata.get(2) + 1);
                       }
                       displayMedalList.put(athlete.getEvent(), medaldata);
                   } else {
                       if (athlete.getMedal().equals("\"Gold\"") || athlete.getMedal().equals("\"Gold\"") || athlete.getMedal().equals("\"Gold\"")) {
                           List<Integer> medalData = new ArrayList<>();
                           medalData.add(0);
                           medalData.add(0);
                           medalData.add(0);
                           if (athlete.getMedal().equals("\"Gold\"")) {
                               medalData.set(0, 1);
                           } else if (athlete.getMedal().equals("\"Silver\"")) {
                               medalData.set(1, 1);
                           } else if (athlete.getMedal().equals("\"Bronze\"")) {
                               medalData.set(2, 1);
                           }
                           displayMedalList.put(athlete.getEvent(), medalData);
                       }
                   }
               }
           }
       }catch (Exception e){
           e.printStackTrace();
       }

       return displayMedalList;
    }

    public static Map<String, Integer> findGoldWinnerInFootballInEveryOlympic(List<Athlete> athletes) {
        Map<String,Integer> goldMedalInFootball = new TreeMap<>();

        try {
            for (Athlete athlete : athletes) {
                if (athlete.getSport().equals("\"Football\"") && athlete.getMedal().equals("\"Gold\"")) {
                    if (goldMedalInFootball.containsKey(athlete.getYear() + " " + athlete.getTeam() + " " + athlete.getMedal())) {
                        goldMedalInFootball.put(athlete.getYear() + " " + athlete.getTeam() + " " + athlete.getMedal(),
                                goldMedalInFootball.get(athlete.getYear() + " " + athlete.getTeam() + " " + athlete.getMedal()) + 1);
                    } else {
                        goldMedalInFootball.put(athlete.getYear() + " " + athlete.getTeam() + " " + athlete.getMedal(), 1);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return goldMedalInFootball;
    }

    public static List<Map.Entry<String, Integer>>  findFemaleHighestGoldWinnerAllOlympic(List<Athlete> athletes) {
        Map<String,Integer> femaleHighestGoldWinner = new TreeMap<>();
        List<Map.Entry<String, Integer>> sortHighestGoldWinner=null;

        try {
            for (Athlete athlete : athletes) {
                if (athlete.getSex().equals("\"F\"") && athlete.getMedal().equals("\"Gold\"")) {
                    if (femaleHighestGoldWinner.containsKey(athlete.getName())) {
                        femaleHighestGoldWinner.put(athlete.getName(), femaleHighestGoldWinner.get(athlete.getName()) + 1);
                    } else {
                        femaleHighestGoldWinner.put(athlete.getName(), 1);
                    }
                }
            }
            sortHighestGoldWinner = new ArrayList<>(femaleHighestGoldWinner.entrySet());
            sortHighestGoldWinner.sort(Comparator.comparing(Map.Entry::getValue));
        }catch (Exception e){
            e.printStackTrace();
        }
            return sortHighestGoldWinner;
    }
    
    public  static Map<String,String> findCountryWhichWonHighestMedalIneachOlympic(List<Athlete> athletes) {
       Map<String,Integer> year = new TreeMap<>();
       Map<String,String> highestMedalWinnerInEachOlympic =new TreeMap<>();

       try {
           for (Athlete athlete : athletes) {
               if (year.containsKey(athlete.getYear())) {
                   year.put(athlete.getYear(), year.get(athlete.getYear()) + 1);
               } else {
                   year.put(athlete.getYear(), 1);
               }
           }
           List<Map.Entry<String, Integer>> sortHighestMedalWinnerTeam = null;

           for (Map.Entry<String, Integer> res : year.entrySet()) {
               Map<String, Integer> HighestMedalWinnerTeam = new TreeMap<>();
               for (Athlete athlete : athletes) {
                   if (athlete.getYear().equals(res.getKey())) {
                       if (athlete.getMedal().equals("\"Gold\"") || athlete.getMedal().equals("\"Silver\"") || athlete.getMedal().equals("\"Bronze\"")) {
                           if (HighestMedalWinnerTeam.containsKey(athlete.getTeam())) {
                               HighestMedalWinnerTeam.put(athlete.getTeam(), HighestMedalWinnerTeam.get(athlete.getTeam()) + 1);
                           } else {
                               HighestMedalWinnerTeam.put(athlete.getTeam(), 1);
                           }
                       }
                   }
                   sortHighestMedalWinnerTeam = new ArrayList<>(HighestMedalWinnerTeam.entrySet());
                   sortHighestMedalWinnerTeam.sort(Comparator.comparing(Map.Entry::getValue));
               }
               if (!sortHighestMedalWinnerTeam.isEmpty()) {
                   highestMedalWinnerInEachOlympic.put(res.getKey(), String.valueOf(sortHighestMedalWinnerTeam.get(sortHighestMedalWinnerTeam.size() - 1)));
               }
           }
       }catch (Exception e){
           e.printStackTrace();
       }
        return highestMedalWinnerInEachOlympic;
    }

    /*public static void findEventViseMedalListInYear1980(List<Athlete> athletes){

        Map<String, Map<String,Integer>>displayMedalList = new TreeMap<>();

        for(Athlete athlete : athletes)
        {
            if(athlete.getYear().equals("1980") && (athlete.getMedal().equals("\"Gold\"") ||athlete.getMedal().equals("\"Silver\"")||athlete.getMedal().equals("\"Bronze\"")))
            {
                if(displayMedalList.containsKey(athlete.getEvent()))
                {
                    if(displayMedalList.get(athlete.getEvent()).containsKey(athlete.getMedal())) {
                        displayMedalList.get(athlete.getEvent()).put(athlete.getMedal(),
                                displayMedalList.get(athlete.getEvent()).get(athlete.getMedal()) + 1);
                    }
                    else {
                        displayMedalList.get(athlete.getEvent()).put(athlete.getMedal(),1);
                    }
                }
                else {
                    HashMap<String,Integer> medalList = new HashMap<>();
                    *//*medalList.put("Gold",0);
                    medalList.put("Silver",0);
                    medalList.put("Bronze",0);*//*
                    *//*if(athlete.getMedal().equals("\"Gold\""))
                    {
                        medalList.put("Gold",1);
                    }
                    else if(athlete.getMedal().equals("\"Silver\""))
                    {
                        medalList.put("Silver",1);
                    } else if(athlete.getMedal().equals("\"Bronze\""))
                    {
                        medalList.put("Bronze",1);
                    }*//*

                    medalList.put(athlete.getMedal(),1);
                    displayMedalList.put(athlete.getEvent(),medalList);
                }
            }
        }

        for (Map.Entry<String,Map<String,Integer>> result : displayMedalList.entrySet()) {
            System.out.println(result.getKey()+"  "+result.getValue());
        }
        System.out.println("=========================================");
    }
*/

    public static List<Athlete> getAthleteData() {
        List<Athlete> athletes = new ArrayList<>();
        String path = "/home/ubuntu/Desktop/Olympic_Project/src/CSV_File/athlete_events.csv";

        try {
            BufferedReader athletesData = new BufferedReader(new FileReader(path));
            athletesData.readLine();

            String line = "";
            while ((line = athletesData.readLine()) != null) {
                String data[] = line.split(",");
                Athlete athlete = new Athlete();

                athlete.setId(data[ID]);
                athlete.setName(data[NAME]);
                athlete.setSex(data[SEX]);
                athlete.setAge(data[AGE]);
                athlete.setHeight(data[HEIGHT]);
                athlete.setWeight(data[WEIGHT]);
                athlete.setTeam(data[TEAM]);
                athlete.setNoc(data[NOC]);
                athlete.setGames(data[GAMES]);
                athlete.setYear(data[YEAR]);
                athlete.setSeason(data[SEASON]);
                athlete.setCity(data[CITY]);
                athlete.setSport(data[SPORT]);
                athlete.setEvent(data[EVENT]);
                athlete.setMedal(data[MEDAL]);

                athletes.add(athlete);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return athletes;
    }
}
